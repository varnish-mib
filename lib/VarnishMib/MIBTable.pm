# This file is part of varnish-mib -*- automake -*-
# Copyright (C) 2018 Sergey Poznyakoff
#
# varnish-mib is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# varnish-mib is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with varnish-mib.  If not, see <http://www.gnu.org/licenses/>.

package VarnishMib::MIBTable;
use strict;
use warnings;
use parent 'VarnishMib::HashTable';
use Carp;

sub new {
    my ($class, $type, $table, %args) = @_;
    my @oids =
	sort { $table->{$a}[1] cmp $table->{$b}[1] }
	grep { $table->{$_}[0] eq 'DICT' } keys %{$table};

    my @equiv;
    my @uniq_oids;
    for (my $i = 0; $i <= $#oids; $i++) {
	unless ($i > 0
		&& $table->{$uniq_oids[-1]}[1] eq $table->{$oids[$i]}[1]) {
	    push @uniq_oids, $oids[$i];
	}
	push @equiv, $#uniq_oids;
    }
    my $self = $class->SUPER::new(%args);
    $self->{oids_all} = \@oids;
    $self->{oids_uniq} = \@uniq_oids;
    $self->{equiv} = \@equiv;
    $self->{type} = $type;

    $self->create([map { $table->{$_}[1] } @{$self->{oids_uniq}}]);
    return $self;
}

sub format_oids {
    my ($self, $fh) = @_;
    $fh ||= \*STDOUT;

    print $fh "enum ".$self->prefix."oid {\n";
    for (my $i = 0; $i <= $#{$self->{oids_all}}; $i++) {
	print $fh $self->indent . $self->{oids_all}[$i];
	if ($i > 0
	    && $self->{oids_all}[$i] ne $self->{oids_uniq}[$self->{equiv}[$i]]) {
	    print $fh  ' = ' . $self->{oids_uniq}[$self->{equiv}[$i]];
	}
	print $fh ",\n";
    }
    print $fh $self->indent . 'MAX_' . uc($self->prefix) . "OID\n";
    print $fh "};\n\n";
}

sub format_program {
    my ($self, $fh) = @_;
    $fh ||= \*STDOUT;
    $self->format_oids($fh);
    $self->SUPER::format_program($self->{type}, $fh);
}

sub str {
    my $self = shift;
    my $str;
    open(my $fh, '>', \$str) or croak "can't write to string";
    $self->format_program($fh);
    close $fh;
    return $str;
};

use overload '""' => \&str;

1;



