#include "varnish_mib.h"
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <string.h>

char *progname;

static void pidfile_check(char const *pid_file);
static void pidfile_create(char const *pid_file);
static int pidfile_remove(char const *pid_file);

void
usage(FILE *fp)
{
	fprintf(fp, "usage: varnish-mib [OPTIONS]\n");
	fprintf(fp, "SNMP agent for Varnish Cache monitoring\n");
	fprintf(fp, "OPTIONS are:\n\n");

	fprintf(fp,
		"  -A          append to the log file rather than truncating it\n");
	fprintf(fp,
		"  -C          do not read any configuration files except the ones optionally\n"
		"              specified by the -c option\n");
	fprintf(fp,
		"  -D TOKEN[,TOKEN...]\n"
		"              turn on debugging output for the given TOKENs\n");
	fprintf(fp,
		"  -H          display a list of configuration file directives understood\n"
		"              by the agent and then exit\n");
	fprintf(fp,
		"  -L[efos]    configure logging\n");
	fprintf(fp,
		"  -c FILE     ead FILE as a configuration file (or a comma-separated list of\n"
		"              configuration files\n");
	fprintf(fp,
		"  -d          dump (in hexadecimal) the sent and received SNMP packets\n");
	fprintf(fp,
		"  -f          remain in the foreground\n");
	fprintf(fp,
		"  -h, -?      print this help summary\n");
	fprintf(fp,
		"  -n NAME     assume this program name\n");
	fprintf(fp,
		"  -p          save the process ID of the daemon in FILE\n");
	fprintf(fp, "\n");
	fprintf(fp, "default configuration path is: %s\n",
		get_configuration_directory());
}

static volatile int stop;

static void
stophandler(int sig)
{
	stop = 1;
}

int
main(int argc, char **argv)
{
	int c, i;
	int foreground = 0;
	struct sigaction sa;
	static int signo[] = {
		SIGHUP,
		SIGINT,
		SIGQUIT,
		SIGTERM,
		0
	};
	int log_set = 0;
	int snmp_dump_packet = 0;
	
	progname = strrchr(argv[0], '/');
	if (progname)
		progname++;
	else
		progname = argv[0];
	if (strncmp(progname, "lt-", 3) == 0)
		progname += 3;

	while ((c = getopt(argc, argv, "ACD:HL:c:dfhn:p:")) != EOF) {
		switch (c) {
		case 'A':
			netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
					       NETSNMP_DS_LIB_APPEND_LOGFILES,
					       1);
			break;

		case 'C':
			netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
					       NETSNMP_DS_LIB_DONT_READ_CONFIGS,
					       1);
			break;
			
		case 'D':
#ifdef NETSNMP_NO_DEBUGGING
			fprintf(stderr, "%s: debugging not configured\n",
				progname);
			exit(1);
#else
			debug_register_tokens(optarg);
			snmp_set_do_debugging(1);
#endif
			break;
			
		case 'H':
			varnish_mib_config_help();
			break;
			
		case 'L':
			if (snmp_log_options(optarg, argc, argv) < 0) {
				usage(stderr);
			}
			log_set = 1;
			break;

		case 'c':
			netsnmp_ds_set_string(NETSNMP_DS_LIBRARY_ID,
					      NETSNMP_DS_LIB_OPTIONALCONFIG,
					      optarg);
			break;

		case 'd':
			netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
					       NETSNMP_DS_LIB_DUMP_PACKET,
					       ++snmp_dump_packet);
			break;
			
		case 'f':
			foreground = 1;
			break;

		case 'n':
			progname = optarg;
			break;

		case 'p':
			pid_file = optarg;
			pid_file_immutable = 1;
			break;

		case 'h':
			usage(stdout);
			exit(0);
			
		default:
			if (optopt == '?') {
				usage(stdout);
				exit(0);
			}
			fprintf(stderr, "%s: unrecognized option: %c\n",
				progname, optopt);
			usage(stderr);
			exit(1);
		}
	}

	netsnmp_ds_set_string(NETSNMP_DS_LIBRARY_ID,
			      NETSNMP_DS_LIB_APPTYPE, progname);
	
	netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID,
			       NETSNMP_DS_AGENT_ROLE, 1);
	if (!log_set)
		snmp_enable_stderrlog();
	
	SOCK_STARTUP;
	init_agent(progname);
	init_varnish_mib();
	init_snmp(progname);
	if (post_config())
		exit(1);

	pidfile_check(pid_file);

	if (!foreground) {
		if (daemon(0, 1)) {
			fprintf(stderr, "%s: daemon: %s\n",
				progname, strerror(errno));
			exit(1);
		}
		openlog(progname, LOG_CONS|LOG_PID, LOG_DAEMON);
		if (!log_set) {
			snmp_enable_syslog_ident(progname, LOG_DAEMON);
			snmp_disable_stderrlog();
		}
	}

	pidfile_create(pid_file);
	
	sa.sa_handler = stophandler;
	sa.sa_flags = 0;
	sigemptyset (&sa.sa_mask);
	for (i = 0; signo[i]; i++)
		sigaction(signo[i], &sa, NULL);

	while (!stop) {
		agent_check_and_process(1);
	}
	
	SOCK_CLEANUP;
	snmp_shutdown(progname);
	pidfile_remove(pid_file);
	return 0;
}

static int
pidfile_remove(char const *pid_file)
{
	if (pid_file && unlink(pid_file)) {
		snmp_log(LOG_ERR,
			 "cannot unlink pidfile `%s': %s\n",
			 pid_file,
			 strerror(errno));
		return 1;
	}
	return 0;
}

static void
pidfile_create(char const *pid_file)
{
	FILE *fp;

	if (!pid_file)
		return;

	fp = fopen(pid_file, "w");
	if (!fp) {
		snmp_log(LOG_CRIT,
			 "cannot create pidfile `%s': %s\n",
			 pid_file,
			 strerror(errno));
		exit(1);
	}
	fprintf(fp, "%lu\n", (unsigned long) getpid());
	fclose(fp);
}

/* Check whether pidfile exists and if so, whether its PID is still
   active. Exit if it is. */
static void
pidfile_check(char const *pid_file)
{
	unsigned long pid;
	FILE *fp;

	if (!pid_file)
		return;

	fp = fopen(pid_file, "r");

	if (fp) {
		if (fscanf(fp, "%lu", &pid) != 1) {
			snmp_log(LOG_ERR,
				 "cannot get pid from pidfile `%s': %s\n",
				 pid_file,
				 strerror(errno));
		} else {
			if (kill(pid, 0) == 0) {
				snmp_log(LOG_ERR,
					 "%s appears to run with pid %lu. "
					 "If it does not, remove `%s' and retry.\n",
					 progname,
					 pid,
					 pid_file);
				exit(1);
			}
		}

		fclose(fp);
		if (pidfile_remove(pid_file))
			exit(1);
	} else if (errno != ENOENT) {
		snmp_log(LOG_CRIT,
			 "cannot open pidfile `%s': %s\n",
			 pid_file, strerror(errno));
		exit(1);
	}
}
