/* This file is part of varnish-mib
   Copyright (C) 2018-2019 Sergey Poznyakoff

   Varnish-mib is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Varnish-mib is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with varnish-mib.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "varnish_mib.h"
#include <ctype.h>

static int
timeout_parser(const char *token, char *line, unsigned *retval)
{
	char *p;
	unsigned long n = strtoul(line, &p, 10);

	if (*p) {
		if (isspace(*p)) {
			while (*p && isspace(*p))
				++p;
			if (*p) {
				config_perror("too many arguments");
				return 1;
			}
		} else {
			config_perror("invalid timeout value");
			return 1;
		}
	}
	
	if (n > UINT_MAX) {
		config_perror("timeout value out of allowed range");
		return 1;
	}
	
	*retval = n;
	return 0;
}

unsigned banTable_timeout = 60;
unsigned vcli_timeout = 5;
unsigned backendTable_timeout = 5;
vcli_sockaddr_t vcli_sockaddr;
char *vcli_secret;
char *pid_file = NULL;
int pid_file_immutable = 0;

static void
ban_table_timeout_parser(const char *token, char *line)
{
	timeout_parser(token, line, &banTable_timeout);
}

static void
backend_table_timeout_parser(const char *token, char *line)
{
	timeout_parser(token, line, &backendTable_timeout);
}

static void
vcli_timeout_parser(const char *token, char *line)
{
	timeout_parser(token, line, &vcli_timeout);
}

static void
vcli_address_parser(const char *token, char *line)
{
	vcli_sockaddr = vcli_parse_sockaddr(line);
}

static void
vcli_address_releaser(void)
{
	free(vcli_sockaddr);
}

static void
vcli_secret_parser(const char *token, char *line)
{
	vcli_secret = strdup(line);
}

static void
vcli_secret_releaser(void)
{
	free(vcli_secret);
}

static void
pidfile_parser(const char *token, char *line)
{
	if (!pid_file_immutable)
		pid_file = strdup(line);
}

static void
pidfile_releaser(void)
{
	/* nothing */
}

struct varnish_mib_config
{
	char *token;
	char *help;
	void (*parser)(const char *token, char *line);
	void (*releaser) (void);
};

static struct varnish_mib_config config[] = {
	{ "banTableTimeout",
	  "SECONDS",
	  ban_table_timeout_parser },
	{ "backendTableTimeout",
	  "SECONDS",
	  backend_table_timeout_parser },
	{ "cliPortTimeout",
	  "SECONDS",
	  vcli_timeout_parser },
	{ "CLISocket",
	  "ADDRESS[:PORT]",
	  vcli_address_parser,
	  vcli_address_releaser },
	{ "CLISecretFile",
	  "FILE",
	  vcli_secret_parser,
	  vcli_secret_releaser },
	{ "pidfile",
	  "FILE",
	  pidfile_parser,
	  pidfile_releaser },
	  
	{ NULL }
};

void
varnish_mib_config_init(void)
{
	struct varnish_mib_config *cp;
	for (cp = config; cp->token; cp++) {
		if (!register_config_handler(progname,
					     cp->token,
					     cp->parser,
					     cp->releaser,
					     cp->help))
			snmp_log(LOG_ERR,"can't register %s config handler\n",
				cp->token);
	}
}

void
varnish_mib_config_help(void)
{
	int i;

	for (i = 0; config[i].token; i++) {
		printf("%s %s\n", config[i].token, config[i].help);
	}
}
