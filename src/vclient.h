struct vcli_sockaddr;
typedef struct vcli_sockaddr *vcli_sockaddr_t;

vcli_sockaddr_t vcli_parse_sockaddr(char const *arg);

typedef struct vcli_conn {
	int fd;
	char *secret;
	int resp;
	char *base;
	size_t bufmax;
	size_t bufsize;
} vcli_conn_t;

int vcli_write(vcli_conn_t *conn);
int vcli_read_response(vcli_conn_t *conn);
int vcli_vasprintf(vcli_conn_t *conn, const char *fmt, va_list ap);
int vcli_asprintf(vcli_conn_t *conn, const char *fmt, ...);
void vcli_disconnect(vcli_conn_t *conn);
int vcli_connect(struct vsm *vsm, vcli_conn_t *conn);

