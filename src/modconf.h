extern char *progname;
extern char *pid_file;
extern int pid_file_immutable;

extern unsigned banTable_timeout;
extern unsigned vcli_timeout;
extern unsigned backendTable_timeout;
extern vcli_sockaddr_t vcli_sockaddr;
extern char *vcli_secret;

void varnish_mib_config_init(void);
void varnish_mib_config_help(void);


