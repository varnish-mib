.\" This file is part of Varnish-mib -*- nroff -*-
.\" Copyright (C) 2014-2019 Sergey Poznyakoff
.\"
.\" Varnish-mib is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Varnish-mib is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Varnish-mib.  If not, see <http://www.gnu.org/licenses/>.
.TH VARNISH-MIB 8 "February 14, 2019" "varnish-mib"
.SH NAME
varnish\-mib \- Net-SNMP agent for Varnish Cache monitoring
.SH SYNOPSIS
\fBvarnish-mib\fR\
 [\fB\-ACHdfh\fR]\
 [\fB\-D \ITOKEN\fR[,\fITOKEN\fR...]]\
 [\fB\-L[eo]\fR]\
 [\fB\-Lf \fIFILE\fR]\
 [\fB\-Ls \fIFACILITY\fR]\
 [\fB\-LE \fIPRI\fR[\fB\-\fIPRI2\fR]]\
 [\fB\-c \fIFILE\fR]\
 [\fB\-n \fINAME\fR]\
 [\fB\-p \fIFILE\fR]
.SH DESCRIPTION
An agent for
.BR snmpd (8)
that provides access to Varnish Cache statistics.  To successfully
couple with the \fBsnmpd\fR the latter must be started with the
.B \-x
command line option, or with the \fBmaster agentx\fR directive in
.BR snmpd.conf (5)
file.
.PP
The values in the OID branches
.BR client ,
.BR total ,
.BR master ,
.BR session ,
.BR threads ", and "
.B objects
are obtained from Varnish API.  
.PP
The OID branches
.BR backend ,
.BR bans ", and "
.B vcl
are obtained using \fBvarnishd\fR administrative interface (similar
to
.BR varnishadm (8)).
.PP
To retrieve this information, the agent must
have enough permissions to scan the Varnish management directory and
read files located in it.  Normally this means that it must be started
either as root or as the user
.B varnishd
is started as.
.PP
The configuration for
.B varnish\-mib
is read from the file
.B varnish\-mib.conf
located in one of the SNMP configuration path directories.  The
following configuration directives are understood:
.TP
\fBpidfile\fR \fIFILE\fR
Upon startup, write the process ID of the \fBvarnish\-mib\fR daemon to
\fIFILE\fR.  See also the \fB\-p\fR command line option, below.
.TP
\fBbanTableTimeout\fR \fINUMBER\fR
To create \fBbanTable\fR (see below), \fBvarnish_mib\fR connects to
\fBvarnish\fR administration port and issues the \fBban.list\fR
command.  To minimize the performance impact, the information obtained
is cached for a predefined amount of time (60 seconds by default).
This amount (in seconds) is configured by \fBvarnishBanTableTimeout\fR
statement.
.TP
\fBbackendTableTimeout\fR \fINUMBER\fR
Update interval for \fBbackendTable\fR.  Default is 5 seconds.
.TP
\fBcliPortTimeout\fR \fINUMBER\fR
Set timeout for I/O operations with Varnish administrative port.
Default is 5 seconds.
.PP
The following two statements are not normally needed, but are
provided for completeness sake:
.TP
\fBcliSocket\fR \fIADDRESS\fR[:\fIPORT\fR]
Set the address of Varnish administrative interface socket.
.TP
\fBcliSecretFile\fR \fIFILE\fR
Set the pathname of the Varnish secret file. 
.SH OPTIONS
.TP
.B \-A
Append to the log file rather than truncating it.
.TP
\fB\-c \fIFILE\fR
Read \fIFILE\fR as a configuration file (or a comma-separated list of
configuration  files).
.TP
.B \-C
Do not read any configuration files except the ones specified by the
\fB\-c\fR option.
.TP
\fB\-d
Dump (in hexadecimal) the sent and received SNMP packets.
.TP
\fB\-D \ITOKEN\fR[,\fITOKEN\fR...]
Turn on debugging output for the given TOKEN(s).  Special token
\fIALL\fR produces extremely verbose output.  Other tokens available
are:
.RS 4
.TP
.B varnish_mib
Produces general debugging information.
.TP
.B ban
Outputs verbose report about loading th ban table.
.TP
.B vcli
Displays additional information regarding varnish
.B CLI
interaction.
.TP
.B vcli:transcript
Enables full transcript of varnish CLI session.
.TP
.B backend
Outputs verbose information about loading the backend information.
.RE
.TP
.B \-f
Remain in the foreground.
.TP
\fB\-h\fR, \fB\-?\fR
Print a short usage summary and exit.
.TP
.B \-H
Print a list of configuration file directives understood by the agent
and then exit.
.TP
\fB\-Le\fR, \fB\-Lo\fR, \fB\-Lf \fIFILE\fR, \fB\-Ls \fIFACILITY\fR, \fB\-LE \fIPRI\fR[\fB\-\fIPRI2\fR]
Specify where logging output should be directed (standard error or
output, to a file or via syslog).  See
.B LOGGING OPTIONS
in
.BR snmpcmd (5)
for details.
.TP
\fB\-n \fINAME\fR
Set an alternative application name (which will affect the
configuration files loaded).  By default it is the same as the name
of the binary.
.TP
\fB\-p \fIFILE\fR
Save the process ID of the daemon in \fIFILE\fR.  This option
overrides the \fBpidfile\fR configuration directive.
.SH OIDS
The following OIDs are defined in the
.B VARNISH-MIB.txt
file:
.SS Branch \(dqclient\(dq
.TP
.B clientAcceptedConnections
Number of accepted connections.
.TP
.B clientRequestsReceived
Number of received HTTP requests.
.TP
.B clientCacheHits
Number of cache hits.  A cache hit indicates that an object has been
delivered to a  client without fetching it from a backend server.
.TP
.B clientCacheHitsPass
Number of hits for pass.  A cache hit for pass indicates that Varnish
passes the request to the backend and this decision itself has been cached. 
.TP
.B clientCacheMisses
Number of misses.  A cache miss indicates the object was fetched from
the backend before delivering it to the client.
.TP
.B clientRequests400
Client requests received, subject to 400 errors.
.TP
.B clientBan
A write-only OID.  When set, invalidates the cache using the supplied
value as argument to ban.  When read, returns an empty string.  E.g.,
to invalidate caches of all \fBpng\fR images:

.EX
snmpset \fIhostname\fR VARNISH\-MIB::clientBan.0 s 'req.url ~ \(dq\\.png$\(dq'
.EE
.SS Branch \(dqbackend\(dq
.TP
.B backendConnSuccess
Number of successful connections to the backend.
.TP
.B backendConnNotAttempted
Number of backend connections not attempted, because of the unhealthy
status of the backend.
.TP
.B backendConnToMany
Number of backend connections failed because there were too many
connections open.
.TP
.B backendConnFailures
Number of failed backend connections.
.TP
.B backendConnReuses
Number of reused backend connections.  This counter is increased
whenever Varnish reuses a recycled connection.
.TP
.B backendConnRecycled
Number of backend connection recycles.  This counter is increased
whenever Varnish has keep-alive connection that is put back into
the pool of connections.  It has not yet been used, but it might be,
unless the backend closes it.
.TP
.B backendConnRetry
Backend connections retried.
.TP
.B backendRequests
Total backend requests made.
.TP
.B backendTable
This branch provides a conceptual table of backends with the
corresponding statistics.  It is indexed by \fBvbeIndex\fR.  Each row
has the following elements:
.RS
.TP
.B vbeIdent
A string uniqiely identifying the backend.
.TP
.B vbeIPv4
IPv4 address of the backend.  Empty if the backend has no IPv4 address.
.TP
.B vbeIPv6
IPv6 address of the backend.  Empty if the backend has no IPv6 address.
.TP
.B vbePort
Port number.
.TP
.B vbeHappyProbes
Number of successful health probes.
.TP 
.B vbeVcls
Number of VCL references.
.TP
.B vbeRequestHeaderBytes
Total number of request header bytes sent to that backend.
.TP
.B vbeRequestBodyBytes
Total number of request body bytes sent to that backend.
.TP
.B vbeResponseHeaderBytes
Total number of response header bytes received from that backend.
.TP
.B vbeResponseBodyBytes
Total number of response body bytes received from that backend.
.TP
.B vbePipeHeaderBytes
Total number of header bytes piped to that backend.
.TP
.B vbePipeIn
Total number of bytes piped to that backend.
.TP
.B vbePipeOut
Total number of bytes piped from that backend.
.RE
.SS Branch \(dqtotal\(dq
.TP
.B totalSessions
Total number of sessions served since the startup.
.TP
.B totalRequests
Total number of requests received since the startup.
.TP
.B totalPipe
Total number of requests piped to the backend.
.TP
.B totalPass
Total number of requests passed to the backend.
.TP
.B totalFetch
Total number of fetches.
.TP
.B totalRequestHeaderBytes
Total request header bytes received.
.TP
.B totalRequestBodyBytes
Total request body bytes received.
.TP
.B totalResponseHeaderBytes
Total header bytes sent out in responses.
.TP
.B totalResponseBodyBytes
Total body bytes sent out in responses.
.TP
.B totalPipeHeaderBytes
Total request header bytes received for piped sessions.
.TP
.B totalPipeIn
Total number of bytes forwarded from clients in pipe sessions.
.TP
.B totalPipeOut
Total number of bytes forwarded to clients in pipe sessions.
.SS Branch \(dqmaster\(dq
.TP
.B uptime
Master daemon uptime, in hundredths of a second.
.SS Branch \(dqsession\(dq
.TP
.B sessAccepted
Number of sessions succesfully accepted.
.TP
.B sessQueued
Number of times session was queued waiting for a thread.
.TP
.B sessDropped
Number of sessions dropped because session queue was full.
.TP
.B sessClosed
Number of sessions closed.
.TP
.B sessPipeline
This OID was used in Varnish \fR4.1\fR, but disappeared from version
\fB5.0\fR.
.TP
.B sessReadAhead
Session read-ahead.
.TP
.B sessHerd
Session herd.
.TP
.B sessDrop
Number of sessions dropped for thread.
.TP
.B sessFail
Number of session accept failures.
.TP
.B sessPipeOverflow
This OID was used in Varnish \fR4.1\fR, but disappeared in version
\fB5.0\fR.
.SS Branch \(dqthreads\(dq
.TP
.B threadsPools
Number of thread pools.
.TP
.B threadsTotal
Number of thread pools.
.TP
.B threadsLimitHits
Number of times more threads were needed, but limit was reached in a
thread pool. 
.TP
.B threadsCreated
Total number of threads created in all pools.
.TP
.B threadsDestroyed
Total number of threads destroyed in all pools.
.TP
.B threadsFailed
Number of times creating a thread failed.
.SS Branch \(dqbans\(dq
.TP
.B bansTotal
Total number of bans.
.TP
.B bansCompleted
Count of completed bans.
.TP
.B bansObj
Number of bans using \fBobj.*\fR.
.TP
.B bansReq
Number of bans using \fBreq.*\fR.
.TP
.B bansAdded
Number of bans added.
.TP
.B bansDeleted
Number of bans deleted.
.TP
.B bansTested
Number of bans tested against objects (lookup).
.TP
.B bansObjectsKilled
Number of objects killed by bans (lookup).
.TP
.B bansLurkerTested
Number of bans tested against objects (lurker).
.TP
.B bansTestTested
Number of ban tests tested against objects (lookup).
.TP
.B bansLurkerTestTested
Number of ban tests tested against objects (lurker).
.TP
.B bansLurkerObjKilled
Number of objects killed by bans (lurker).
.TP
.B bansDups
Number of ans superseded by other bans.
.TP
.B bansLurkerContention
Number of times lurker gave way for lookup.
.TP
.B bansPersistedBytes
Number of bytes used by the persisted ban lists.
.TP
.B bansPersistedFragmentation
Extra bytes in persisted ban lists due to fragmentation.
.TP
.B banTable
A table of configured varnish bans.  It is indexed by the
\fBbanIndex\fR OID.  Each row has the following elements:
.RS
.TP
.B banTime
Time when the ban was set.
.TP
.B banRefCount
Number of references to that ban.  This equals to the number of objects
in the varnish cache affected by that ban.
.TP
.B banExpression
VCL expression of the ban.
.RE

Notice that for performance reasons, the ban table is cached, so the
total number of rows in the \fBbanTable\fR may diverge from the value
of \fBbansTotal\fR variable.  The default update interval is 60
seconds.  It can be configured in the \fBsnmpd.conf\fR file
(see the \fBvarnishBanTableTimeout\fR statement above).
.SS Branch \(dqobjects\(dq
.TP
.B objectsCount
Approximate number of HTTP objects (headers + body, if present) in the cache.
.TP
.B objectsVampire
Number of unresurrected objects.
.TP
.B objectsCore
Approximate number of object metadata elements in the cache. Each
object needs an objectcore, extra objectcores are for
hit-for-miss, hit-for-pass and busy objects.
.TP
.B objectsHead
Approximate number of different hash entries in the cache.
.TP    
.B objectsExpired
Number of objects that expired from cache because of old age.
.TP    
.B objectsLRUNuked
How many objects have been forcefully evicted from storage to make
room for a new object. 
.TP
.B objectsLRUMoved
Number of move operations done on the LRU list.
.TP    
.B objectsPurges
Number of purge operations executed.
.TP
.B objectsObjPurged
.TP
.B objectsGzip
Number of gzip operations.
.TP    
.B objectsGunzip
Number of gunzip operations.
.SS Branch \(dqvcl\(dq
.TP
.B vclTotal
Number of loaded VCLs in total.
.TP
.B vclAvail
Number of VCLs available.
.TP    
.B vclDiscard
Number of discarded VCLs.
.TP    
.B vclFail
Number of VCL failures.
.SS Branch \(dqagent\(dq
The \fBagent\fR branch is reserved for
implementation-specific management.  It is not used currently.
.SH NOTES
The following OIDs were used in Varnish 4. They are no longer
available in newer Varnish releases:
.TP
.B clientRequests411
Client requests received, subject to 411 errors.
.TP
.B clientRequests413
Client requests received, subject to 413 errors.
.TP
.B backendConnUnused
Number of unused backend connections.
.PP
The OIDs
.B clientRequestsReceived
and
.B totalRequests
return the same value.
.SH "SEE ALSO"
.BR snmpd.conf (5),
.BR snmpd (8),
.BR snmpcmd (1),
.BR varnish (7),
.BR varnishstat (1).
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2014-2019 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:


