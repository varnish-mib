typedef struct be_string {
	char const *start;
	size_t len;
} be_string_t;

typedef void (*regfun_t)(be_string_t *, be_string_t *, be_string_t *, void *);

struct vsm;
struct VSC_point;
void backend_register(char const *name, size_t len, char const *param,
		      const struct VSC_point *vpt);
void backend_clear(void);
int backend_collect_addr(struct vsm *vsm);
void backend_parser(const char *str, size_t len, regfun_t regfun, void *d);

